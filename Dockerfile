FROM gradle:latest as builder

COPY --chown=gradle:gradle . /home/gradle/src

WORKDIR /home/gradle/src

RUN gradle build

FROM adoptopenjdk/openjdk14:ubi

RUN mkdir -p /sb-keycloak-app/src

ARG JAR_FILE=/home/gradle/src/build/libs/app-0.0.1-SNAPSHOT.jar

WORKDIR mkdir /sb-keycloak-app/src

COPY --from=builder ${JAR_FILE} springboot-keycloak-app-v1.jar

ADD src/main/resources/application-dev.yml //

EXPOSE 8000

# Tell docker to run the application
ENTRYPOINT ["java", "-jar", "-Dspring.profiles.active=dev", "springboot-keycloak-app-v1.jar"]

