#!/bin/bash

# This is a setup bash for docker containers!
## activate via: chmod 0755 setup_bash or chmod +x setup_bash
## navigate to wd docker_contents
## execute in terminal via source ./setup_bash

echo ""
echo "Welcome, You are executing a setup script bash for transaction service docker containers."
echo ""

# image name: springboot-keycloak-app:v1
docker build . -t springboot-keycloak-app:v1
