# Integrating SpringBoot with Keycloak

## Envoy, OPA
### gRPC
- In gRPC, a client application can directly call a method on a server application on a different machine as if it were a local object, making it easier for you to create distributed applications and services.
- Like many RPC systems, gRPC is based around the idea of defining a service, specifying the methods that can be called remotely with their parameters and return types.
    - https://grpc.io/docs/what-is-grpc/core-concepts/
    - Remote Procedure Call (RPC) is a protocol that one program can use to request a service from a program located in another computer on a network without having to understand the network's details. 
## To do
- JWT filter to obtain teams instead of in userInfo() in userController
    - Filter ordering does not help to get user principal (after FilterSecurityInterceptor)
    - Possible by using get("resource_access") -> need to check if this is the right way to get teams
        - Have to see best practices to update teams in user that will be used in different places 

## Minikube deployment
1. Setup docker image in minikube VM
2. Exit minikube VM, run `kubectl` accordingly 
3. `minikube service app` to run service in browser / get URL to run BE service

### Envoy and OPA as sidecars of application
1. Create configMap containing configuration of envoy



## Resources 
- [Spring Security Tutorial](https://www.toptal.com/spring/spring-security-tutorial)
- [Deploy Spring Boot applications with Minikube](https://www.baeldung.com/spring-boot-minikube)
