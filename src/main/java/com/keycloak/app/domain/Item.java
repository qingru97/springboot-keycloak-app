package com.keycloak.app.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@AllArgsConstructor
public class Item {

    @NotNull
    private String name;
    private float budget;
    private String brand;
    private String description;
}
