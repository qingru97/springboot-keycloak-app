//package com.keycloak.app.config;
//
//import com.keycloak.app.util.JwtTokenUtil;
//import lombok.RequiredArgsConstructor;
//import org.springframework.http.HttpHeaders;
//import org.springframework.stereotype.Component;
//import org.springframework.web.filter.OncePerRequestFilter;
//
//import javax.servlet.FilterChain;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.util.List;
//import java.util.Map;
//
//import static ch.qos.logback.core.util.OptionHelper.isEmpty;
//
//@Component
//@RequiredArgsConstructor
//public class JwtTokenFilter extends OncePerRequestFilter {
//
//    private final JwtTokenUtil jwtTokenUtil;
//
//    @Override
//    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
//            FilterChain filterChain) throws ServletException, IOException {
//        final String header = request.getHeader(HttpHeaders.AUTHORIZATION);
//
//        if (isEmpty(header) || !header.startsWith("Bearer ")) {
//            filterChain.doFilter(request, response);
//            return;
//        }
//        // Get jwt token and validate
//        final String token = header.split(" ")[1].trim();
//        if (!jwtTokenUtil.validate(token)) {
//            filterChain.doFilter(request, response);
//            return;
//        }
//        final Map<String, Object> appResourceAccess =
//                (Map<String, Object>) ((Map<String, Object>) jwtTokenUtil.getClaims(token)
//                        .get("resource_access")).get("app");
//        List<String> teams = (List<String>) appResourceAccess.get("roles");
//
//        filterChain.doFilter(request, response);
//    }
//
//}
