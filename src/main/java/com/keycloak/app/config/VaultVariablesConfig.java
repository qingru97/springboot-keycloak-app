package com.keycloak.app.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.vault.core.VaultKeyValueOperationsSupport;
import org.springframework.vault.core.VaultTemplate;
import org.springframework.vault.support.VaultResponse;

@Component
public class VaultVariablesConfig {

    @Autowired
    private VaultTemplate vaultTemplate;

    public String getValueFromVaultKey(String key) {
        VaultResponse response = vaultTemplate
                .opsForKeyValue("secret", VaultKeyValueOperationsSupport.KeyValueBackend.KV_2)
                .get("app");
        return String.valueOf(response.getData().get(key));
    }
}
