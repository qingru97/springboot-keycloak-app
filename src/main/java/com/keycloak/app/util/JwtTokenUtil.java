//package com.keycloak.app.util;
//
//import io.jsonwebtoken.*;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import java.security.PublicKey;
//import java.util.Date;
//
//@Component
//@Slf4j
//public class JwtTokenUtil {
//
//    @Autowired
//    private PublicKey publicKey;
//
//    public Claims getClaims(String token) {
//        Claims claims = Jwts.parser().setSigningKey(publicKey).parseClaimsJws(token).getBody();
//        return claims;
//    }
//
//    public String getUserId(String token) {
//        Claims claims = Jwts.parser().setSigningKey(publicKey).parseClaimsJws(token).getBody();
//        return claims.getSubject().split(",")[0];
//    }
//
//    public String getUsername(String token) {
//        Claims claims = Jwts.parser().setSigningKey(publicKey).parseClaimsJws(token).getBody();
//        return claims.getSubject().split(",")[1];
//    }
//
//    public Date getExpirationDate(String token) {
//        Claims claims = Jwts.parser().setSigningKey(publicKey).parseClaimsJws(token).getBody();
//        return claims.getExpiration();
//    }
//
//    public boolean validate(String token) {
//        try {
//            Jwts.parser().setSigningKey(publicKey).parseClaimsJws(token);
//            return true;
//        } catch (SignatureException ex) {
//            log.error("Invalid JWT signature - {}", ex.getMessage());
//        } catch (MalformedJwtException ex) {
//            log.error("Invalid JWT token - {}", ex.getMessage());
//        } catch (ExpiredJwtException ex) {
//            log.error("Expired JWT token - {}", ex.getMessage());
//        } catch (UnsupportedJwtException ex) {
//            log.error("Unsupported JWT token - {}", ex.getMessage());
//        } catch (IllegalArgumentException ex) {
//            log.error("JWT claims string is empty - {}", ex.getMessage());
//        }
//        return false;
//    }
//
//}
