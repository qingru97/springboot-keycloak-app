package com.keycloak.app.controller;

import com.google.common.collect.ImmutableList;
import com.keycloak.app.domain.Item;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Slf4j
@RequestMapping("/item")
public class ItemController {

    @GetMapping
    public List<Item> getItemsByUser() {
        System.out.println("Return list of items for user");
        return ImmutableList.of(new Item("Chocolate", 1.5F, "Hershey", "Taste good"));
    }
}
