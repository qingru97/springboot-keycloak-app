package com.keycloak.app.controller;

import com.google.common.collect.ImmutableList;
import com.keycloak.app.domain.UserInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@Slf4j
@RequestMapping("/user")
public class UserController {

    @GetMapping(value = "/users")
    public ResponseEntity<UserInfo> getUser() {
        return ResponseEntity.ok(new UserInfo("usertest", ImmutableList.of("teamA", "teamB")));
    }

    //    @RolesAllowed("Apple")
    //    @GetMapping(value = "/users")
    //    public ResponseEntity<String> getUser(@RequestHeader String Authorization) {
    //        return ResponseEntity.ok("Hello User");
    //    }
    //
    //    // Principal object takes in bearer access token in authorization header
    //    @GetMapping(value = "/userinfo")
    //    public ResponseEntity<UserInfo> handleUserInfoRequest(Principal principal) {
    //        if (principal instanceof KeycloakAuthenticationToken) {
    //            SimpleKeycloakAccount details =
    //                    (SimpleKeycloakAccount) ((KeycloakAuthenticationToken) principal).getDetails();
    //            Collection<GrantedAuthority> authorities =
    //                    ((KeycloakAuthenticationToken) principal).getAuthorities();
    //            List<String> teams = authorities.stream().map(GrantedAuthority::getAuthority)
    //                    .collect(Collectors.toList());
    //            return ResponseEntity.ok(new UserInfo("usertest", teams));
    //        }
    //        return null;
    //    }
}

