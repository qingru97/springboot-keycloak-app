kind: Deployment
apiVersion: apps/v1
metadata:
  name: kc-app
  labels:
    app: kc-app
spec:
  replicas: 1
  selector:
    matchLabels:
      app: kc-app
  template:
    metadata:
      labels:
        app: kc-app
    spec:
      initContainers:
        - name: proxy-init
          image: openpolicyagent/proxy_init:v5
          # Configure the iptables bootstrap script to redirect traffic to the
          # Envoy proxy on port 8080, specify that Envoy will be running as user
          # 1111, and that we want to exclude port 8282 from the proxy for the
          # OPA health checks. These values must match up with the configuration
          # defined below for the "envy" and "opa" containers.
          args: [ "-p", "8080", "-u", "1111", "-w", "8282" ]
          securityContext:
            capabilities:
              add:
                - NET_ADMIN
            runAsNonRoot: false
            runAsUser: 0

      containers:
        - name: app
          image: springboot-keycloak-app:v1
          ports:
            - containerPort: 8000
          imagePullPolicy: IfNotPresent
        - name: envoy
          image: envoyproxy/envoy:v1.10.0
          securityContext:
            runAsUser: 1111
          volumeMounts:
            - readOnly: true
              mountPath: /config
              name: proxy-config
          args:
            - "envoy"
            - "--log-level"
            - "debug"
            - "--config-path"
            - "/config/envoy.yaml"
        - name: opa
          # Note: openpolicyagent/opa:latest-istio is created by retagging
          # the latest released image of OPA-Istio.
          image: openpolicyagent/opa:0.22.0-istio
          securityContext:
            runAsUser: 1111
          volumeMounts:
            - readOnly: true
              mountPath: /policy
              name: opa-policy
          args:
            - "run"
            - "--server"
            - "--addr=localhost:8181"
            - "--diagnostic-addr=0.0.0.0:8282"
            - "--set=plugins.envoy_ext_authz_grpc.addr=:9191"
            - "--set=plugins.envoy_ext_authz_grpc.query=data.envoy.authz.allow"
            - "--set=decision_logs.console=true"
            - "--ignore=.*"
            - "/policy/policy.rego"
          livenessProbe:
            httpGet:
              path: /health?plugins
              scheme: HTTP
              port: 8282
            initialDelaySeconds: 5
            periodSeconds: 5
          readinessProbe:
            httpGet:
              path: /health?plugins
              scheme: HTTP
              port: 8282
            initialDelaySeconds: 5
            periodSeconds: 5
      volumes:
        - name: proxy-config
          configMap:
            name: proxy-config
        - name: opa-policy
          secret:
            secretName: opa-policy
